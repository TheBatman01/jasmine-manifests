# jasmine-manifest
Copyright 2017 - 2019 The LineageOS Project

Device configuration for ZTE Trek 2 HD
======================================

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Quad-core 1.5 GHz ARM® Cortex™ A53 and quad-core 1.2 GHz ARM® Cortex™ A53
CHIPSET | Qualcomm MSM8952 Snapdragon 617
GPU     | Adreno 405
Memory  | 2 GB
Shipped Android Versions | 6.0.1, 7.1.1
Storage | 16 GB
MicroSD | Up to 128 GB
Battery | 4600 mAh (non-removable)
Dimensions | 214.8 x 125.0 x 8.9 mm
Display | 1280 x 800 pixels, 8.0" AMOLED
Rear Camera  | 5.0 MP, dual-LED flash
Front Camera | 5.0 MP
Release Date | August 2016

![ZTE Trek 2 HD](https://d28dq596ebml6z.cloudfront.net/media/catalog/product/cache/1/image/400x506.32911392405/9df78eab33525d08d6e5fb8d27136e95/k/8/k88_front.jpg)
